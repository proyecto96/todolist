//
//  NotesViewModel.swift
//  ToDoList
//
//  Created by Mai Rebollar on 09/06/22.
//

import Foundation
import SwiftUI

final class NotesViewModel: ObservableObject {
    @Published var notes: [NoteModel] = []
    
    init() {
        notes = getAllNotes()
    }
    
    func saveNote(description: String) {
        let newNote = NoteModel(description:  description)
        notes.insert(newNote, at: 0)
        encodeAndSaveAllNotes()
    }
    
    private func encodeAndSaveAllNotes() {
        if let encoded = try? JSONEncoder().encode(notes) {
            UserDefaults.standard.set(encoded, forKey: "notes")
        }
    }
    
    func getAllNotes() -> [NoteModel] {
        if let notesData = UserDefaults.standard.object(forKey: "notes") as? Data {
            if let notes = try? JSONDecoder().decode([NoteModel].self, from: notesData) {
                return notes
            }
        }
        return []
    }

    func removeNote(whereId id: String) {
        notes.removeAll(where: { $0.id == id })
        encodeAndSaveAllNotes()
    }
    
    func updateFavoriteNote(note: Binding<NoteModel>) {
        note.wrappedValue.isFavorited = !note.wrappedValue.isFavorited
        encodeAndSaveAllNotes()
    }
    
    func getNumberOfNotes() -> String {
        "\(notes.count)"
    }
    
    func showAlert(title: String, message: String, actionDone: UIAlertAction,context:UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(actionDone)
        context.present(alert, animated: true, completion: nil)
    }
    
    func isEmpty() {
        if notes.isEmpty {
//            let mensaje = ""
//            showAlert(title: "Mensaje", message: "Ocurrió un error al obtener el formulario \(mensaje ?? "")", actionDone: UIAlertAction(title: "Aceptar", style: .default), context: self)
        }
    }
}

